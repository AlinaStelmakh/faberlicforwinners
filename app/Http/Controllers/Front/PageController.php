<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Winner;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class PageController extends Controller {


    protected $title_email = 'Новое сообщение с сайта';
    public function __construct()
    {
        $this->data = array();
    }
    public function index()
    {
        return view('frontend.home', $this->data);
    }

    public function showPage($page)
    {
        if(!empty($page) && $page == "winners"){
            $winner = new Winner();
            $this->data['winners'] = $winner->paginateAllWinners();
        }
        return view('frontend.'.$page, $this->data);
    }
    public function showAllWinners(Winner $winner)
    {
        $winners = $winner->getAllWinners();
        return view('frontend.inc.load_winners',['winners' => $winners])->render();
    }

    protected $rules = array(
        'name'  => 'required|max:255',
        'email' => 'required|email|max:255',
        'phone' => 'required',
        'city' => 'required',
        'url'   => 'required|url',
        'ref'   => 'required|max:255',
        'count' => 'in:0,1,2,3,4',
        'consult_number' => 'required_if:check_consult,1',
    );

    protected $messages = array(
        'name.required'  => 'Поле "Имя" должно быть заполнено!',
        'name.max:255'   => 'Поле "Имя" должно состоять MAX из 255 символов',
        'city.required'  => 'Поле "Город" должно быть заполнено!',
        'phone.required'  => 'Поле "Телефон" должно быть заполнено!',
        'email.required' => 'Поле "E-mail" должно быть заполнено!',
        'email.max:255'  => 'Поле "E-mail"должно состоять MAX из 255 символов',
        'email.email'    => 'E-mail должен быть действительным адресом электронной почты',
        'url.required'   => 'Поле "Ссылка на пост" должно быть заполнено!',
        'url.url'        => 'Поле "Ссылка на пост" должно быть действительной ссылкой',
        'ref.required'   => 'Поле "Артикул" должно быть заполнено!',
        'ref.max:255'    => 'Поле "Артикул" должно состоять MAX из 255 символов',
        'count.in'    => 'Эта ссылка была уже сохранена, используйте другую, пожалуйста',
        'consult_number.required_if'    => 'Поле "Номер Консультанта" должно быть заполнено!',
    );

    public function storeParticipant()
    {
        $response = ['success' => false, 'message' => ''];
        $data  = Input::all();
        $data['count'] = strval(Winner::where('url','like', $data['url'])->count());

        $pattern = "/^http(s)?:\/\/.*/i";
        if(!preg_match($pattern, $data['url']))
        {
            $data['url']= 'https://' . $data['url'];
        }
        // dd($data);
        $validator = Validator::make($data, $this->rules, $this->messages);

        if($validator->fails())
        {
            $response['success'] = false;
            $response = $validator->errors();
            //dd( $response);
            return response()->json($response);
        }
        Winner::create($data);
        $response['success'] = true;

        return response()->json($response);
    }

    public function contact()
    {
        $response = ['success' => false, 'message' => ''];
        $data  = Input::all();
        $messages = [
            'message.required' => 'Поле "Ваше сообщение" должно быть заполнено и состоять минимум из 10 символов!',
            'message.min' => 'Сообщениие должно состоять минимум из 10 символов',
            'email.required' => 'Поле "E-mail" должно быть заполнено!',
            'email.email' => 'E-mail должен быть действительным адресом электронной почты',
        ];

        $validator = Validator::make($data, [
            'email' =>   'required|email',
            'message' => 'required|min:10',
        ], $messages);

        if($validator->fails())
        {
            $response['success'] = false;
            $response = $validator->errors();

            return response()->json($response);
        }

        Contact::create($data);

        \Mail::send('backend.includes.email-template', ['data' => $data], function( $message ) use( $data ) {
            $message->from(env('MAIL_USERNAME'), env('MAIL_NAME'))
                ->to(env('FABERLIC_ADMIN_EMAIL'), env('FABERLIC_ADMIN_NAME'))
                ->subject($this->title_email);
        });
        $response['success'] = true;
        return response()->json($response);
    }


}
