<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\URL;
use App\Http\Requests;

class ContactFormController extends Controller
{
    public function __construct()
    {
        $this->data =  collect([
            'title',
            'route',
            'submit_text',
            'features',
            'route_feature',
            'route_cancel',
            'include',
        ]);
    }

    public function index()
    {
        $data = $this->data->push('table-head')->combine(['Запросы формы обратной связи',
            '','','','','','backend.contacts.index',
            ['Дата', 'Сообщение','E-mail',  'Опции']
        ]);
        return view('backend.includes.index-table', compact('data', 'records'));
    }

    protected $rules =array(
        /* 'nome' => 'required',*/
    );

    public function show($id)
    {
        $record = Contact::findOrFail($id);
        $data = $this->data->combine(['Сообщение',
            '','','','','','',
        ]);
        return view('backend.contacts.show', compact('data', 'record'));
    }

    public function destroy($id)
    {
        $row = Contact::findOrFail($id);
        $row->delete();
    }

    public function anyData()
    {
        $records = DB::table('contacts')->select('contacts.*')->orderBy('created_at','desc');

        return Datatables::of($records)
            ->addColumn('option', function ($record) {
                $app_url = URL::to('/');
                $crf_token = csrf_token();
                return '<form method="POST" action="'.$app_url.'/admin/contact_form/'.$record->id.'" accept-charset="UTF-8" style="display: inline-table;">
                        <input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'. $crf_token.'"><meta id="token" name="token" content="'. $crf_token.'">
                        <a role="button" class="btn btn-danger btn-xs deleteRow" data-toggle="modal" data-row_id="'.$record->id.'" data-row_destroy_route="'.$app_url.'/admin/contact_form/'.$record->id.'">
                        <i class="fa fa-trash-o"></i></a></form><a class="btn btn-success btn-xs" role="button" href="'.$app_url.'/admin/contact_form/'.$record->id.'">
                        <i class="fa fa-comment-o"></i></a>';
            })

            ->editColumn('date', function ($record) {
                return substr ($record->updated_at, 0, -9);

            })
            ->editColumn('message', function ($record) {
                $string = $record->message;
                $description = strip_tags($string);

                if (strlen($description) > 50) {
                    $text = wordwrap($description, 50);

                    $text = substr($text, 0, strpos($text, "\n"));

                } else {
                    $text = $description;
                }
                return $text. "...";

            })
            ->rawColumns(['option'])
            ->make(true);

    }
}
