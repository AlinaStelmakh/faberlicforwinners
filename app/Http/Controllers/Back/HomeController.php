<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests;
use App\Models\Contact;
use App\Models\Winner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(Winner $winner)
    {
        $this->data = array();
        $this->data['participant_all']= Winner::all()->count();
        $this->data['winners'] = $winner->getAllWinners()->count();
        $this->data['contacts'] = Contact::all()->count();
        //dd($this->data);
        return view('backend.includes.dashboard', $this->data);
    }
}
