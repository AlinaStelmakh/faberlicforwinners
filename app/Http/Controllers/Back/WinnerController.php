<?php

namespace App\Http\Controllers\Back;

use App\Models\Winner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;


class WinnerController extends Controller
{
    const NAME = 'winner';



    protected $rules =array(
        'name'  => 'required|max:255',
        'email' => 'required|email|max:255',
        'url'   => 'required|url',
        'phone' => 'required',
        'city'  => 'required',
        'ref'   => 'required|max:255',
        'count'  =>   'in:0,1,2,3,4',
    );

    protected $messages =array(
        'name.required'  => 'Поле "Имя" должно быть заполнено!',
        'name.max:255'   => 'Поле "Имя" должно состоять MAX из 255 символов',
        'email.required' => 'Поле "E-mail" должно быть заполнено!',
        'email.max:255'  => 'Поле "E-mail"должно состоять MAX из 255 символов',
        'email.email'    => 'E-mail должен быть действительным адресом электронной почты',
        'url.required'   => 'Поле "Ссылка на пост" должно быть заполнено!',
        'url.url'        => 'Поле "Ссылка на пост" должно быть действительной ссылкой',
        'ref.required'   => 'Поле "Артикул" должно быть заполнено!',
        'ref.max:255'    => 'Поле "Артикул" должно состоять MAX из 255 символов',
        'count.in'    => 'Эта ссылка была уже сохранена, используйте другую, пожалуйста',
    );


    public function __construct()
    {
        $this->data =  collect([
            'title',
            'route',
            'submit_text',
            'features',
            'route_feature',
            'route_cancel',
            'include',
        ]);
    }

    public function index()
    {
        $data = $this->data->push('table-head')->combine(
            ['Участники Розыгрыша','winners.data', 'Выбрать победителя','','','','backend.'.self::NAME.'s.index', ['id','Имя', 'E-mail', 'Артикул',  'Дата', 'Ссылка','Статус', 'Опции']
            ]);
        return view('backend.includes.index-table', compact('data'));
    }


    public function edit($id)
    {
        $record = Winner::with('media')->findOrFail($id);
        $data = $this->data->push('feature_name')->combine(
            ['Редактировать учасника',
                self::NAME.'s.update',
                'обновить',
                'drop-zone',
                'media.store',
                self::NAME.'s.index',
                'backend.'.self::NAME.'s._form',
                'img',
            ]);
        return view('backend.includes.form', compact('data', 'record'));

    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $request['count'] = strval(Winner::where('url','like', $input['url'])->count());
        $this->validate($request, $this->rules, $this->messages);
        $record = Winner::findOrFail($id);

        if( !empty( $request['status'] ) && $request['status'] == 1 && empty( $record['date'] )){
            $input['date'] =  Carbon::now()->toDateTimeString();
        }
        //dd($input);
        $record->update($input);
        return redirect('admin/'.self::NAME.'s')->with('alert', 'Participant has been updated');
    }


    public function destroy($id)
    {
        $row = Winner::findOrFail($id);
        $row->delete();
    }


    public function sendEmail($id)
    {
        $row = Winner::findOrFail($id);
        $row->update(['send_email' => 1]);

        \Mail::send('backend.includes.email-template_winner', ['data' => $row], function( $message ) use( $row ) {
            $message->from(env('MAIL_USERNAME'), env('MAIL_NAME'))
                ->to($row->email, $row->name)
                ->subject('Конкурс Faberlic: Мое желание!');
        });
        $response['success'] = true;
        return response()->json($response);
    }



    public function allData(Request $request)
    {

        $records = Winner::selectRaw('distinct participants.*');
        $app_url = URL::to('/');
        $crf_token = csrf_token();

        return Datatables::eloquent( $records)
            ->filter(function ($query) use ($request) {

                if ($request->has('winner') && $request['winner'] == 1) {
                    $random_id='';
                    $random = DB::table('participants')->where([['status', '!=', 1]])->inRandomOrder()->first();
                    if( $random != null){
                        $random_id = $random->id;
                    }
                    $query->where([['status', '!=', 1],['id', '=', $random_id]]);
                }

            })
            ->addColumn('option', function ($record) use ($app_url, $crf_token){
                $crf_token = csrf_token();
                if( $record->status == 0){
                    return'<form method="POST" action="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'" accept-charset="UTF-8" style="display: inline-table;"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'. $crf_token.'"><meta id="token" name="token" content="'. $crf_token.'"><a role="button" class="btn btn-danger btn-xs deleteRow" data-row_id="'.$record->id.'" data-row_destroy_route="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'">
                        <i class="fa fa-trash-o"></i></a></form><a class="btn btn-success btn-xs" role="button" href="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'/edit">
                        <i class="fa fa-pencil"></i></a>';
                }
                else {
                    if( $record->send_email == 0){
                        return'<form method="POST" action="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'" accept-charset="UTF-8" style="display: inline-table;"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'. $crf_token.'"><meta id="token" name="token" content="'. $crf_token.'"><a role="button" class="btn btn-danger btn-xs deleteRow" data-row_id="'.$record->id.'" data-row_destroy_route="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'">
                        <i class="fa fa-trash-o"></i></a><a class="btn btn-success btn-xs" role="button" href="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'/edit">
                        <i class="fa fa-pencil"></i></a></form><form method="POST" action="'.$app_url.'/admin/send_email/'.$record->id.'" accept-charset="UTF-8" style="display: inline-table;"><input name="_token" type="hidden" value="'. $crf_token.'"><meta id="token" name="token" content="'. $crf_token.'"><a role="button" class="btn btn-info btn-xs sendEmail" data-row_id="'.$record->id.'" data-row_send_route="'.$app_url.'/admin/send_email/'.$record->id.'">
                        <i class="fa fa-paper-plane"></i></a></form>';
                    }else{
                        return'<form method="POST" action="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'" accept-charset="UTF-8" style="display: inline-table;"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'. $crf_token.'"><meta id="token" name="token" content="'. $crf_token.'"><a role="button" class="btn btn-danger btn-xs deleteRow" data-row_id="'.$record->id.'" data-row_destroy_route="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'">
                        <i class="fa fa-trash-o"></i></a><a class="btn btn-success btn-xs" role="button" href="'.$app_url.'/admin/'. self::NAME .'s/'.$record->id.'/edit">
                        <i class="fa fa-pencil"></i></a></form><a role="button" class="btn btn-info btn-xs disabled sendEmail" data-row_id="'.$record->id.'" data-row_send_route="'.$app_url.'/admin/send_email/'.$record->id.'">
                        <i class="fa fa-share-square-o"></i></a>';
                    }

                }
            })
            ->editColumn('status', function ($record) {
                if( $record->status == 0){
                    return 'зарегистрированный';
                }
                else {
                    return 'победитель';
                }
            })
            ->editColumn('date', function ($record) {
                if( $record->date != null){
                    return $record->date;
                }
            })
            ->editColumn('url', function ($record) {
                return '<a class="btn-link" href="'.$record->url.'"  target="_blank">перейти</i></a>';
            })
            ->rawColumns(['option','url'])

            ->make(true);
    }

}
