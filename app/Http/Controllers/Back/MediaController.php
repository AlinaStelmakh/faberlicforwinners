<?php

namespace App\Http\Controllers\Back;

use App\Models\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;

class MediaController extends Controller
{

    protected $rules =array(
        /* 'nome' => 'required',*/
    );

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $file = Input::file('image');
        $destinationPath_original = public_path() . '/uploads/admin/';
        $destinationPath_round = public_path() . '/uploads/admin/rounds/';
        $destinationPath_thumb = public_path() . '/uploads/admin/thumbs/';
        $fileName = md5(date("y-m-d h-m-s")).'-'.$file->getClientOriginalName();
        Input::file('image')->move($destinationPath_original, $fileName);
        $image = Image::make($destinationPath_original.'/'.$fileName);
        $width = $image->width();
        $height = $image->height();

        if($width > $height){
            $image->resize(null, 248, function ($constraint) {
                $constraint->aspectRatio();
            })->crop(326, 248)->save($destinationPath_thumb . $fileName);

            $image->resize(null, 182, function ($constraint) {
                $constraint->aspectRatio();
            })->crop(182, 182)->save($destinationPath_round . $fileName);

        }else{
            $image->resize(182, null, function ($constraint) {
                $constraint->aspectRatio();
            })->crop(182, 182)->save($destinationPath_round . $fileName);
            $image->resize(326, null, function ($constraint) {
                $constraint->aspectRatio();
            })->crop(326, 248)->save($destinationPath_thumb . $fileName);
        }


        $media = new Media();
        $media->title = $fileName;
        $media->name  = $file->getClientOriginalName();
        $media->save();
        session()->put('media', $media->id);
        return $media;
    }

    public function deleteUpload(Request $request)
    {
        $input = $request->all();
        $row = Media::where([
                ['id', '=', $input['id']],
            ])->firstOrFail();
        \File::delete(public_path() . '/uploads/admin/'. $row->title);
        \File::delete(public_path() . '/uploads/admin/rounds/'. $row->title);
        \File::delete(public_path() . '/uploads/admin/thumbs/'. $row->title);
        $row->delete();
    }
    public function destroy($id)
    {
        $row = Media::findOrFail($id);
        \File::delete(public_path() . '/uploads/admin/'. $row->title);
        \File::delete(public_path() . '/uploads/admin/rounds/'. $row->title);
        \File::delete(public_path() . '/uploads/admin/thumbs/'. $row->title);
        $row->delete();
        return redirect()->back()->withInput();
    }
}
