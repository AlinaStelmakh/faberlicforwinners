<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAccountService
{
    public function createOrGetUser( ProviderUser $providerUser, $provider)
    {

        $user = User::whereEmail($providerUser->getEmail())->first();
        if (!$user) {
            $user = User::create([
                'email' => $providerUser->getEmail(),
                'name' => $providerUser->getName(),
                'provider_user_id' => $providerUser->getId(),
                'avatar' => $providerUser->getAvatar(),
                'provider' => $provider
            ]);

        }
        return $user;
    }
}