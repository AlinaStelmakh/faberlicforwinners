<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{

    protected $table = 'participants';

    protected $fillable = [
        'name',
        'email',
        'phone',
        'city',
        'url',
        'ref',
        'date',
        'status',
        'img',
        'send_email',
        'consult_number',
    ];

    public function media()
    {
        return $this->hasOne('App\Models\Media', 'id', 'img');
    }
    public function paginateAllWinners()
    {
        return $this->where('status', '=', 1)->orderBy('date','decs')->paginate(13);
    }
    public function getAllWinners()
    {
        return $this->where('status', '=', 1)->orderBy('date','decs')->get();
    }

    protected static function boot() {
        parent::boot();
        static::created(function ($winner) {
            \Mail::send('frontend.inc.email-template', ['data' => $winner], function($message) use ($winner){
                $message->from(env('MAIL_USERNAME'), env('MAIL_NAME'))
                    ->to($winner->email, $winner->name)
                    ->subject('Faberlic');
            });
        });
    }
}
