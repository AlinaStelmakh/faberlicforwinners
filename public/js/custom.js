function sameHeights(selector) {
    var selector = selector || '[data-key="sameHeights"]', query = document.querySelectorAll(selector), elements = query.length, max = 0;
    if (elements) {
        while (elements--) {
            var element = query[elements];
            if (element.scrollHeight > max) {
                max = element.scrollHeight;
            }
        }
        elements = query.length;

        while (elements--) {
            var element = query[elements];
            element.style.height = max + 'px';
        }
    }
}
if ('addEventListener' in window) {
    window.addEventListener('resize', function () {
        if (window.innerWidth > 767) {
            sameHeights('.home-page-step');
            sameHeights('.all-winners-list li');
            //sameHeights('.prizes-banner .badge');
        }
    });
    window.addEventListener('load', function () {
        if (window.innerWidth > 767) {
            sameHeights('.home-page-step');
            sameHeights('.all-winners-list li');
            //sameHeights('.prizes-banner .badge');
        }
    });
}
if ('addEventListener' in window) {
    window.addEventListener('load', function () {
        var currentPageClass = jQuery('body').attr('class');
        jQuery('ul.main-nav li a.' + currentPageClass).toggleClass('current-menu-item');
    });
}
jQuery('.show-popup').click(function (e) {
    e.preventDefault();
    (jQuery)(".wait").empty();
    var screenID = jQuery(this).attr('screenID'), form = (jQuery)('form').show();
    jQuery('#' + screenID).fadeIn();
});
jQuery('.close-popup').click(function (e) {
    e.preventDefault();
    var screenID = jQuery(this).attr('screenID');
    jQuery('#' + screenID).fadeOut();
    (jQuery)('input, textarea').not("input[type='hidden']").not("input[type='submit']").val('');
    if (navigator.userAgent.search("Chrome") >= 0) {
        (jQuery)('input:-webkit-autofill').each(function () {
            (jQuery)(this).after(this.outerHTML).remove();
            (jQuery)('input[name=' + (jQuery)(this).attr('name') + ']').val((jQuery)(this).val());
        });
    }
    jQuery('.form-group').removeClass('has-error');
    jQuery('.help-block').remove();
});
var strength1 = 30;
var strength2 = 70;
jQuery("html").mousemove(function (e) {
    var pageX = e.pageX - ($(window).width() / 2);
    var newvalueX = 1 * pageX * -1;
    jQuery('#parallax-layer-bottom').css("background-position", (strength1 / jQuery(window).width() * pageX * -1) + "px 0px");
    jQuery('#parallax-layer-up').css("background-position", (strength2 / jQuery(window).width() * pageX * -1) + "px 0px");
});
(jQuery)(document).on('submit', '#contact-form', function (e) {
    var form = (jQuery)('#contact-form');
    jQuery('.form-group').removeClass('has-error');
    jQuery('.help-block').remove();
    e.preventDefault();
    (jQuery)(".wait").html('<i class="fa fa-spinner fa-pulse fa-5x" style="color: #d60058;"></i>').show();
    form.hide();
    (jQuery).ajax({
        url: form.attr('action'), type: 'post', data: form.serialize(), dataType: 'json', success: function (json) {
            if (json.success) {
                (jQuery)(".wait").empty().html('<span class="help-block"><strong style="color:#0b0">Ваш запрос успешно отправлен!</strong></span>');
                (jQuery)('input, textarea').not("input[type='hidden']").not("input[type='submit']").val('');
                if (navigator.userAgent.search("Chrome") >= 0) {
                    (jQuery)('input:-webkit-autofill').each(function () {
                        (jQuery)(this).after(this.outerHTML).remove();
                        (jQuery)('input[name=' + (jQuery)(this).attr('name') + ']').val((jQuery)(this).val());
                    });
                }
                setTimeout(function () {
                    jQuery('#feedback-screen').fadeOut(200);
                }, 3000);
            }
            else {
                (jQuery)(".wait").hide();
                form.show();
                if ('email' in json) {
                    jQuery('.form-group-email').addClass('has-error');
                    jQuery('#email-span').append('<span class="help-block"><strong>' + json.email[0] + '</strong></span>');
                } else {
                    jQuery('.form-group-email').addClass('has-success');
                }
                if ('message' in json) {
                    jQuery('.form-group-ms').addClass('has-error');
                    jQuery('#ms-span').append('<span class="help-block"><strong>' + json.message[0] + '</strong></span>');
                } else {
                    jQuery('.form-group-ms').addClass('has-success');
                }
            }
        }
    });
    return false;
});
(jQuery)(document).on('submit', '#registration-form', function (e) {
    var form = (jQuery)('#registration-form');
    jQuery('.form-group').removeClass('has-error');
    jQuery('.help-block').remove();
    e.preventDefault();
    form.hide();
    (jQuery)(".wait").html('<i class="fa fa-spinner fa-pulse fa-5x" style="color: #d60058;"></i>').show();
    (jQuery).ajax({
        url: form.attr('action'), type: 'post', data: form.serialize(), dataType: 'json', success: function (json) {
            if (json.success) {
                (jQuery)(".wait").empty().html('<span class="help-block"><strong style="color:#0b0">Ваш запрос успешно отправлен!</strong></span>');
                (jQuery)('input, textarea').not("input[type='hidden']").not("input[type='submit']").val('');
                if (navigator.userAgent.search("Chrome") >= 0) {
                    (jQuery)('input:-webkit-autofill').each(function () {
                        (jQuery)(this).after(this.outerHTML).remove();
                        (jQuery)('input[name=' + (jQuery)(this).attr('name') + ']').val((jQuery)(this).val());
                    });
                }
                setTimeout(function () {
                    jQuery('#registration-screen').fadeOut(200);
                }, 3000);
            }
            else {
                (jQuery)(".wait").hide();
                form.show();
                if ('name' in json) {
                    jQuery('.form-group-name').addClass('has-error');
                    jQuery('#name-span').append('<span class="help-block"><strong>' + json.name[0] + '</strong></span>');
                } else {
                    jQuery('.form-group-name').addClass('has-success');
                }
                if ('email' in json) {
                    jQuery('.form-group-email').addClass('has-error');
                    jQuery('#email-span-r').append('<span class="help-block"><strong>' + json.email[0] + '</strong></span>');
                } else {
                    jQuery('.form-group-email').addClass('has-success');
                }
                if ('phone' in json) {
                    jQuery('.form-group-phone').addClass('has-error');
                    jQuery('#phone-span').append('<span class="help-block"><strong>' + json.phone[0] + '</strong></span>');
                } else {
                    jQuery('.form-group-phone').addClass('has-success');
                }
                if ('city' in json) {
                    jQuery('.form-group-city').addClass('has-error');
                    jQuery('#city-span').append('<span class="help-block"><strong>' + json.city[0] + '</strong></span>');
                } else {
                    jQuery('.form-group-city').addClass('has-success');
                }
                if ('url' in json) {
                    jQuery('.form-group-url').addClass('has-error');
                    jQuery('#url-span').append('<span class="help-block"><strong>' + json.url[0] + '</strong></span>');
                }
                if ('ref' in json) {
                    jQuery('.form-group-ref').addClass('has-error');
                    jQuery('#ref-span').append('<span class="help-block"><strong>' + json.ref[0] + '</strong></span>');
                } else {
                    jQuery('.form-group-ref').addClass('has-success');
                }
                if ('consult_number' in json) {
                    jQuery('.form-group-consult').addClass('has-error');
                    jQuery('#onsult-span').append('<span class="help-block"><strong>' + json.consult_number[0] + '</strong></span>');
                }
                if ('count' in json) {
                    jQuery('.form-group-url').addClass('has-error');
                    jQuery('#url-span').append('<span class="help-block"><strong>' + json.count[0] + '</strong></span>');
                }
            }
        }
    });
    return false;
});
jQuery('#all-winners-link').click(function (e) {
    e.preventDefault();
    (jQuery).get(APP_URL + '/' + 'ajax_all_winners', function (data) {
        (jQuery)('.all-winners-list').fadeOut(200, function () {
            (jQuery)('.all-winners-list').empty().append(data).fadeIn();
        });
    });
});

(jQuery)(document).on('change', ':checkbox', function (e) {
//alert(this.checked);
    $('#collapseConsult').collapse('toggle');
    if(this.checked){
        this.value = 1;
    }else{
        this.value = 1;
    }
});