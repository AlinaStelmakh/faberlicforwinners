<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'Back\HomeController@index']);

        Route::resource('winners', 'Back\WinnerController');
        Route::get('ajax_winners_all', ['as' => 'winners.data', 'uses' =>  'Back\WinnerController@allData']);
        Route::post('send_email/{id}', ['as' => 'winners.send', 'uses' =>  'Back\WinnerController@sendEmail']);

        Route::resource('contact_form', 'Back\ContactFormController');
        Route::get('ajax_contact_form_requests_all', ['as' => 'contact_form_requests.data', 'uses' =>  'Back\ContactFormController@anyData']);

        Route::resource('media', 'Back\MediaController');
        Route::post('upload/delete', ['as' => 'upload-remove', 'uses' =>'Back\MediaController@deleteUpload']);
        Route::get('upload/delete', [ 'uses' =>'Back\MediaController@deleteUpload']);

    });
});

Route::get('/', array('as' => 'homepage', 'uses' => 'Front\PageController@index'));
Route::get('ajax_all_winners', array( 'uses' => 'Front\PageController@showAllWinners'));

Route::post('contact-form', array('as' => 'contact.form','uses' => 'Front\PageController@contact'));
Route::post('participant', array('as' => 'participant.store','uses' => 'Front\PageController@storeParticipant'));
Route::get('admin',  function () {
    return redirect('admin/dashboard');
});
Route::get('{static}', array('uses' => 'Front\PageController@showPage'))->where('static', '(about|faq|prizes|rules|winners)');
;
