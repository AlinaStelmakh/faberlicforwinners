@if($errors->any())
    <div class="row text-center">
        <ul class="list-unstyled alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif