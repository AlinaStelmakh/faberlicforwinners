@extends('layouts.front')
@section('title', 'Призы |')
@push('stylesheets')@endpush

@section('main_container')
    <div class="container-fluid hidden-xs">
        <div class="prizes-banner">
            <div class="badge">
                <img src="{{ asset('/images/badge-bg.png') }}" alt="">
                <div class="badge-text">
                    <div class="badge-text-wrapper">
                        <span class="hundred cormorant">~100~</span>
                        <div class="line-sep-01"></div>
                        самых желанных<br>
                        <b>подарков</b><br>
                        <span class="cormorant">каждую неделю!</span>
                        <div class="icon-prize"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid hidden-sm  hidden-md hidden-lg">
        <div class="prizes-banner-mobile">
            <img src="{{ asset('/images/prizes-bg.jpg') }}" alt="" width="100%">
            <div class="badge-text-wrapper">
                <span class="hundred cormorant">~100~</span>
                <div class="line-sep-01"></div>
                самых желанных<br>
                <b>подарков</b><br>
                <span class="cormorant">каждую неделю!</span>
                <div class="icon-prize"></div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')@endpush