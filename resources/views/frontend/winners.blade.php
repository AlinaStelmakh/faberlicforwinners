@extends('layouts.front')
@section('title', 'Победители |')

@push('stylesheets')@endpush

@section('main_container')
    <div class="container-fluid hidden-xs">
        <div class="winners-of-week">
            <div class="badge">
                <img src="{{ asset('/images/badge-bg.png') }}?090517" alt="">
                <div class="badge-text">
                    <div class="badge-text-wrapper">
                        <div class="icon-trof"></div>
                        <div class="pobed">победители</div>
                        <div class="line-sep-01"></div>
                        <span class="cormorant">Поздравляем!</br>Скоро ваш желаемый</br>продукт окажется</br>в ваших руках</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid hidden-sm  hidden-md hidden-lg">
        <div class="winners-banner-mobile">
            <img src="{{ asset('/images/winners-banner-bg.jpg') }}?090517" alt="" width="100%">
            <div class="badge-text-wrapper">
                <div class="icon-trof"></div>
                <div class="pobed">победители</div>
                <div class="line-sep-01"></div>
                <span class="cormorant larger">Поздравляем!</br>Скоро ваш желаемый</br>продукт окажется</br>в ваших руках</span>
            </div>
        </div>
    </div>

    <div class="container all-winners">
            <div class="row text-center">
                @if($winners->count() > 0)
                    <a class="btn btn-white week_winner">победители недели</a>
                @else
                    <a class="black">Первые победители появятся через неделю</a>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="all-winners-list">
                        @forelse($winners as $winner)
                            <li>
                                <img src="@if(empty($winner->media)){{ asset('images/week-winner-photo.jpg') }} @else {{URL::asset('uploads/admin/thumbs/'.$winner->media->title) }} @endif"
                                     alt="">
                                <div class="name">{!! $winner->name !!}</div>
                            </li>
                        @empty
                        @endforelse
                        @if($winners->count() % 3 == 2)
                            <li class="hidden-xs"></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

@endsection

@push('scripts')@endpush