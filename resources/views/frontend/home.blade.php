@extends('layouts.front')

@push('stylesheets')@endpush

@section('main_container')
    <div>
        <div class="home-page-banner hidden-xs">
            <div class=" parallax-container">
                <img src="{{ asset('/images/parallax-bg.jpg') }}" alt="" class="all-in-one">
                <div class="parallax-layer" id="parallax-layer-bottom"></div>
                <div class="parallax-layer" id="parallax-layer-up"></div>
            </div>
            <div class="badge-and-button">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4 col-md-4 text-center">
                            <div class="red-badge">
                                <div class="text">
                                    <div class="wish">Мое желание!</div>
                                    <img src="{{ asset('/images/logo-small-white-2.png') }}" alt="">
                                    <div class="make-it-real cormorant">исполняет желания!</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-4 text-center">
                            <a href="#" class="btn btn-white-text i-am-in show-popup" screenID="registration-screen"> УЧАСТВОВАТЬ</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="mobile-home-page-banner hidden-md hidden-lg hidden-sm">
            <div class="red-badge-container">
                <div class="red-badge">
                    <div class="text">
                        <div class="wish">Мое желание!</div>
                        <img src="{{ asset('/images/logo-small-white-2.png') }}" alt="">
                        <div class="make-it-real cormorant">исполняет желания!</div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-white-text i-am-in show-popup" screenID="registration-screen">УЧАСТВОВАТЬ</a>
        </div>
    </div>

    <div class="container">
        <div class="row home-page-steps">
            <div class="col-sm-4 home-page-step step1">
                <div class="step-icon"></div>
                <div class="step-title">СДЕЛАЙ СКРИНШОТ</div>
                <div class="step-text">3D-изображения любимого продукта* в каталоге Faberlic с помощью приложения Faberlic3D</div>
                <div class="apps-links">
                    <div class="col-md-6 col-sm-12 text-center"><a href="https://play.google.com/store/apps/details?id=com.faberlic.ar&hl=ru
" target="_blank" class="app-link googleplay"></a></div>
                    <div class="col-md-6 col-sm-12 text-center"><a href="https://itunes.apple.com/us/app/faberlic-3d/id1188191622?mt=8
" target="_blank" class="app-link appstore"></a></div>
                </div>
            </div>
            <div class="col-sm-4 home-page-step step2">
                <div class="step-icon"></div>
                <div class="step-title">ПОДЕЛИСЬ</div>
                <div class="step-text">скриншотом в социальных сетях, поставь хэштег #faberlic3D и расскажи о своем самом желанном продукте от Faberlic</div>
            </div>
            <div class="col-sm-4 home-page-step step3">
                <div class="step-icon"></div>
                <div class="step-title">ЗАРЕГИСТРИРУЙСЯ</div>
                <div class="step-text">на сайте Конкурса и получи шанс исполнить свое желание!</div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')@endpush