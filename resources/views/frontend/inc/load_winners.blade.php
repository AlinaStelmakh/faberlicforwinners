@foreach($winners as $index => $winner)
    @if($index > 2)
        <li>
            <img src="@if(empty($winner->media)){{ asset('images/week-winner-photo.jpg') }} @else {{URL::asset('uploads/admin/thumbs/'.$winner->media->title) }} @endif"
                 alt="">
            <div class="name">{!! $winner->name !!}</div>
        </li>
    @endif
@endforeach