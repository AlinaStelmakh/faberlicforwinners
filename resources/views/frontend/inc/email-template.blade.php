<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Faberlic</title>
</head>
<body>
<center>
    <!--[if mso]>
    <table cellpadding="0" cellspacing="0" border="0"><tr><td width="700">
    <![endif]-->
    <table style="max-width:600px;padding:20px" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="left" valign="bottom" width="50%" style="border-bottom:1px solid #d60058;height:40px;padding-bottom:15px;"><img width="126" height="26" src="http://faberlic.breezemedia.eu/images/logo.png" /></td>
            <td align="right" valign="bottom" width="50%" style="border-bottom:1px solid #d60058;height:40px;padding-bottom:15px;"><img class="text_img" width="62" height="13" src="http://faberlic.breezemedia.eu/images/text.png"></td>
        </tr>
        <tr>
            <td colspan="2">
                <h1 style="margin:50px 0;width:100%;text-align:center;font-size:24px;font-weight:normal;font-family:Arial,sans-serif;">Дорогие коллеги,</h1>
                <p style="font-family:Arial,sans-serif;font-size:14px;line-height:150%;margin-bottom:35px;color:#484848;">Просим вас не остаться равнодушными и помочь сделать нашу корпоративную жизнь комфортнее.</p>
                <p style="font-family:Arial,sans-serif;font-size:14px;line-height:150%;margin-bottom:35px;color:#484848;">Пожалуйста, пройдите два коротких опроса: <a href="#" target="_blank" style="color:#0072c6;text-decoration:underline;">Опрос 1</a> и <a href="#" target="_blank" style="color:#0072c6;text-decoration:underline;">Опрос 2</a></p>
                <p style="font-family:Arial,sans-serif;font-size:14px;line-height:150%;margin-bottom:35px;color:#484848;">Оба опроса не займут у вас более 2-х минут. Нам важно Ваше мнение!</p>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" style="padding-top:15px;"><img  width="140" height="140" src="http://faberlic.breezemedia.eu/images/img_bottom.jpg"></td>
        </tr>
    </table>
    <!--[if mso]>
    </td></tr></table>
    </center>
    <![endif]-->
</center>
</body>
</html>
