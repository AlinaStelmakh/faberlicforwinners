@extends('layouts.front')
@section('title', 'Правила |')

@push('stylesheets')@endpush

@section('main_container')
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="rules rule1">
                    <div class="rule-num">1</div>
                    <div class="rule-title">Сделай скриншот</div>
                    <div class="rule-text">3D-изображения любимого продукта в каталоге Faberlic с помощью приложения Faberlic3D. Для этого загрузи приложение  в Google play и Аpp Store. </div>
                    <div class="next-rule"></div>
                </div>
                <div class="rules rule2">
                    <div class="rule-num">2</div>
                    <div class="rule-title">Опубликуй </div>
                    <div class="rule-text">скриншот в своей социальной сети. Напиши, почему этот продукт для тебя самый желанный. Поставь хэштег <b>#faberlic3D</b></div>
                    <div class="next-rule"></div>
                </div>
                <div class="rules rule3">
                    <div class="rule-num">3</div>
                    <div class="rule-title">Зарегистируйся </div>
                    <div class="rule-text">для этого введи свое имя, email, телефон, город, ссылку на пост в социальных сетях, артикул желаемого продукта Faberlic и номер Консультанта (если есть) в специальных полях и нажми «Отправить».</div>
                    <div class="next-rule"></div>
                </div>
                <div class="rules rule4">
                    <div class="rule-num">4</div>
                    <div class="rule-title">Ожидай </div>
                    <div class="rule-text">имейл с подтверждением своего участия в розыгрыше и номером участника</div>
                    <div class="next-rule"></div>
                </div>
                <div class="rules rule5">
                    <div class="rule-num">5</div>
                    <div class="rule-title">Проверяй</div>
                    <div class="rule-text">страницу «Победители». Каждый понедельник будут опубликованы победившие номера.</div>
                    <div class="next-rule"></div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('scripts')@endpush