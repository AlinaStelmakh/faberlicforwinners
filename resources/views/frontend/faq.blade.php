@extends('layouts.front')
@section('title', 'Ответы на вопросы |')
@push('stylesheets')@endpush

@section('main_container')
    <div class="container">
        <div class="panel-group" id="faq-container" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question01">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer01" aria-expanded="true" aria-controls="answer01">
                            Как принять участие в Конкурсе?
                        </a>
                    </h4>
                </div>
                <div id="answer01" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="question01">
                    <div class="panel-body">
                        <p>Для того чтобы принять участие в Конкурсе #faberlic3D, необходимо в период с 9 по 29 октября с помощью приложения Faberlic3D сделать скриншот желаемого продукта из каталога, опубликовать его в своей социальной сети, написать фразу «Faberlic исполняет желания» и поставить хэштег #faberlic3D.</p>
                        <p>Далее, необходимо зарегистрироваться на сайте Конкурса, введя свое имя, email, телефон, город, ссылку на свой пост и артикул желаемого подарка и нажать «Отправить».</p>
                        <p>Артикул продукта должен совпадать с продуктом из публикации в социальных сетях.</p>
                        <p>Публикация в соцсети должна быть доступна для публичного просмотра (не закрыта настройками приватности).</p>
                        <p>После прохождения регистрации и нажатии кнопки «Отправить», участники получают сообщение с подтверждением участия по электронной почте, указанной при регистрации.</p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question02">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer02" aria-expanded="true" aria-controls="answer02">
                            Как сделать скриншот экрана?
                        </a>
                    </h4>
                </div>
                <div id="answer02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question02">
                    <div class="panel-body">
                        <p><strong>Iphone:</strong></p>
                        <p>Для того чтобы сделать снимок Вашего экрана – скриншот, на телефонах с системой IOS необходимо нажать одновременно клавиши HOME (круглая кнопка внизу экрана) и кнопку блокировки (в верхнем торце). Скриншот автоматически сохранится вместе с остальными снимками.</p></br>
                        <p><strong>Android:</strong></p>
                        <p>Большинство телефонов с системой Android позволяют сделать скриншот, одновременно нажав кнопки питания и уменьшения громкости.</p>
                    </div>
                </div>
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question022">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer022" aria-expanded="true" aria-controls="answer022">
                            Какие посты могут участвовать в Конкурсе?
                        </a>
                    </h4>
                </div>
                <div id="answer022" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question022">
                    <div class="panel-body">
                        <p>В Конкурсе может участвовать один пост в выбранной социальной сети, содержащий один скриншот, текст и хэштег Конкурса #faberlic3D. Скриншот должен соответствовать условиями Конкурса. <br/>Вам может быть отказано в участии в Конкурсе, если ваша публикация не соответствует тематике и условиям Конкурса (в частности, не содержит скриншота товара, фразы или хэштега).</p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question03">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer03" aria-expanded="true" aria-controls="answer03">
                            Как поставить хэштег?
                        </a>
                    </h4>
                </div>
                <div id="answer03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question03">
                    <div class="panel-body">
                        <p>Для того чтобы поставить хэштег, необходимо использовать знак # и сразу за ним без пробела написать faberlic3D. Так мы сможем найти Ваш пост не только по ссылке, но и по хэштегу #faberlic3D. Для того чтобы мы смогли увидеть Вашу публикацию, убедитесь, что она доступна для публичного просмотра (не закрыта настройками приватности).</p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question04">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer04" aria-expanded="true" aria-controls="answer04">
                            Как получить ссылку на мой пост?
                        </a>
                    </h4>
                </div>
                <div id="answer04" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question04">
                    <div class="panel-body">
                        <p><strong>VK.com и Одноклассники:</strong></p>
                        <p>Необходимо кликнуть на саму заметку или на время ее публикации. Ссылку можно будет скопировать, выбрав пункт меню «Копировать ссылку» или из адресной строки браузера.</p></br>
                        <p><strong>Facebook:</strong></p>
                        <p>Необходимо нажать на дату поста или его время, если он опубликован недавно. Пост откроется в отдельном окне. Ссылка на него будет находиться в меню, которое легко открыть, нажав на три точки в верхнем углу экрана и выбрать пункт «Копировать ссылку» или скопировать ссылку в адресной строке браузера.</p></br>
                        <p><strong>Instagram:</strong></p>
                        <p>Ссылка на Ваш пост в мобильном приложении находится в меню, скрытом за «флажком» в правом углу фотографии. Также ссылку можно скопировать в адресной строке браузера.</p></br>
                        <p>Для того чтобы мы смогли увидеть Вашу публикацию, убедитесь, что она доступна для публичного просмотра (не закрыта настройками приватности).</p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question05">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer05" aria-expanded="true" aria-controls="answer05">
                            Что писать при регистрации в поле «У меня есть Консультант», если его нет?
                        </a>
                    </h4>
                </div>
                <div id="answer05" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question05">
                    <div class="panel-body">
                        <p>Данное поле не является обязательным, если у Вас нет Консультанта, и его можно пропустить.</p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question06">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer06" aria-expanded="true" aria-controls="answer06">
                            Какие призы предусмотрены за участие в Конкурсе?
                        </a>
                    </h4>
                </div>
                <div id="answer06" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question06">
                    <div class="panel-body">
                        <p>В Конкурсе участвуют только продукты компании Faberlic, которые доступны в приложении Faberlic3D. Каждый победитель получит в подарок продукт, строго соответствующий введенному артикулу. Если скриншот победителя отличается от артикула, выигранный продукт все равно определяется по артикулу и поменять его после отправки регистрационной формы нельзя.</p>
                        <p>Также в качестве призов в Конкурсе разыгрываются iPhone 6s 32Гб.</p>
                    </div>
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question07">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer07" aria-expanded="true" aria-controls="answer07">
                            Как происходит выбор победителей Конкурса?
                        </a>
                    </h4>
                </div>
                <div id="answer07" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question07">
                    <div class="panel-body">
                        <p>Каждую неделю победителями становятся 103 публикаций участников. Победители Конкурса определяются случайным порядком, с помощью генератора случайных чисел.</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question08">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer08" aria-expanded="true" aria-controls="answer07">
                            Как узнать о победе в Конкурсе?
                        </a>
                    </h4>
                </div>
                <div id="answer08" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question08">
                    <div class="panel-body">
                        <p>Участник узнает о победе в Конкурсе из сообщения по электронной почте, указанной при регистрации. Победителю Конкурса необходимо сообщить организаторам адрес для отправки приза ответным сообщением. Если победитель Конкурса не выходит на связь через электронную почту, представители Faberlic связываются с ним по телефону или через социальную сеть.  </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question09">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer09" aria-expanded="true" aria-controls="answer07">
                            Каким образом происходит выдача призов?
                        </a>
                    </h4>
                </div>
                <div id="answer09" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question09">
                    <div class="panel-body">
                        <p>Призы будут отправлены победителям Конкурса на ближайший к его проживанию Пункт выдачи Faberlic. Представитель Faberlic сообщает победителю о поступлении подарка на Пункт выдачи, где он хранится в течение 7 дней с даты доставки. <br/>iPhone доставляется победителю лично в руки от представителя Faberlic. Подтверждая участие в Конкурсе, участник соглашается на публикацию его фото в случае выигрыша iPhone. Фото с победителем делается представителем Faberlic в момент награждения.</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="question10">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#faq-container" href="#answer10" aria-expanded="true" aria-controls="answer07">
                            Что если я живу в другой стране и хочу участвовать?
                        </a>
                    </h4>
                </div>
                <div id="answer10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question10">
                    <div class="panel-body">
                        <p>Участвовать в Конкурсе могут только граждане РФ. В противном случае Вам будет отказано в участии в Конкурсе, даже если Вы попали в число победителей.  </p>
                    </div>
                </div>
            </div>

        </div>
        <div class="text-center">
            <a href="#" class="btn btn-red show-popup" screenID="feedback-screen" id="ask-new-question-button">Задать вопрос</a>
        </div>
    </div>

@endsection

@push('scripts')@endpush