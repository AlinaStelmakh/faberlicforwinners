<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="ru-RU">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WL2XZ3L');</script>
<!-- End Google Tag Manager -->
    <title> @yield('title') Faberlic </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link type="text/css" rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
    <link type="text/css" rel="stylesheet" href="{{ asset("css/style.css") }}?180517-02">
    @stack('stylesheets')
</head>
<body class="page-home">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WL2XZ3L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-9 col-xs-7">
                <a href="{{url('/')}}" class="logo-home-link"><img src="{{ asset('/images/logo-header.png') }}" alt="logo"></a>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-3 col-xs-5">
                <nav class="top-navigation-bar pull-right main-navigation" id="site-navigation" >
                    <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"></button>
                    <ul class="main-nav">
                        <li><a href="#" class="show-popup" screenID="registration-screen">Регистрация</a></li>
                        <li><a href="{{ url('/prizes') }}" class="page-prizes" @if(url('/prizes') == url()->current()) style="color:#d30759"@endif>Призы</a></li>
                        <li><a href="{{ url('/rules') }}" class="page-rules" @if(url('/rules') == url()->current()) style="color:#d30759"@endif>Правила</a></li>
                        <li><a href="{{ url('/winners') }}" class="page-winners" @if(url('/winners') == url()->current()) style="color:#d30759"@endif>победители</a></li>
                        <li><a href="{{ url('/faq') }}" class="page-faq" @if(url('/faq') == url()->current()) style="color:#d30759"@endif>Ответы на вопросы</a></li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>
</header>

<div class="main-content">
    @yield('main_container')
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <ul class="footer-social-links">
                    <li><a href="javascript: void(0);" data-layout="button" onclick="window.open('https://vk.com/share.php?url={{url()->current()}}&title=Конкурс&nbsp;Faberlic&nbsp;&quot;Мое&nbspжелание&quot;&image={{ asset('/images/fb-share.jpg') }}','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');" class="vk"></a></li>
                    <li><a href="javascript: void(0);" data-layout="button" onclick="window.open('https://www.facebook.com/sharer.php?u={{url()->current()}}&title=Конкурс&nbsp;Faberlic&nbsp;&quot;Мое&nbspжелание&quot;&picture={{ asset('/images/fb-share.jpg') }}','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');" class="fb"></a></li>
                    <li><a href="javascript: void(0);" data-layout="button" onclick="window.open('https://connect.ok.ru/offer?url={{url()->current()}}&title=Конкурс&nbsp;Faberlic&nbsp;&quot;Мое&nbspжелание&quot;&imageUrl={{ asset('/images/fb-share.jpg') }}');" class="od"></a></li>
                    <li><a href="javascript: void(0);" data-layout="button" onclick="window.open('https://twitter.com/share?text=Конкурс&nbsp;Faberlic&nbsp;&quot;Мое&nbspжелание&quot;&url={{url()->current()}}');" class="tw"></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">Поделиться с друзьями</div>
            <div class="col-md-4 footer-links-container">
                <div class="row">
                    <div class="col-sm-6"><a href="{{url('/about')}}" class="footer-links" @if(url('/about') == url()->current()) style="color:#d30759"@endif>О бренде</a></div>
                    <div class="col-sm-6"><a href="#" class="footer-links show-popup" screenID="feedback-screen">Обратная связь</a></div>
                    <div class="col-sm-12"><a href="{{asset('/rules.pdf')}}" target="blank_" class="footer-links">Положение о Конкурсе</a></div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="popup-bg" id="registration-screen">
    <div class="popup-box">
        <button class="close-popup" screenID="registration-screen"></button>
        <div class="form-container">
            <div class="popup-title">Регистрация</div>
            <div class="text-center wait"  style="display:none;"><i class="fa fa-spinner fa-pulse fa-5x" style="color: #d60058;"></i></div>
            <form action="{{route('participant.store')}}" method="post" class="forms registration-form" id="registration-form">
                {{ csrf_field() }}
                <div class="form-group form-group-name">
                    <div class="col-md-12"  id="name-span">
                        <input type="text" class="form-control" placeholder="Имя" name="name">
                    </div>
                </div>
                <div class="form-group form-group-email">
                    <div class="col-md-12"  id="email-span-r">
                        <input type="text" class="form-control" placeholder="Email" name="email">
                    </div>
                </div>
                <div class="form-group form-group-phone">
                    <div class="col-md-12"  id="phone-span">
                        <input type="text" class="form-control" placeholder="Телефон" name="phone">
                    </div>
                </div>
                <div class="form-group form-group-city">
                    <div class="col-md-12"  id="city-span">
                        <input type="text" class="form-control" placeholder="Город" name="city">
                    </div>
                </div>
                <div class="form-group form-group-url">
                    <div class="col-md-12"  id="url-span">
                        <input type="text" class="form-control" placeholder="Ссылка на пост" name="url">
                    </div>
                </div>
                <div class="form-group form-group-ref">
                    <div class="col-md-12"  id="ref-span">
                        <input type="text" class="form-control" placeholder="Артикул желаемого товара" name="ref">
                    </div>
                </div>
                <div class="form-group checkbox">
                    <input type="checkbox" id="collapse_consult" name="check_consult" value="0">
                    <label>У меня есть Консультант</label>
                </div>
                <div class="form-group form-group-consult collapse" id="collapseConsult">
                    <div class="col-md-12 "  id="consult-span">
                        <input type="text" class="form-control" placeholder="Номер Консультанта" name="consult_number">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 line-sep-02">
                        <p>Заполните все поля и нажмите "Отправить"</p>
                        <p>Нажимая «Отправить», я соглашаюсь с <a href="{{url('/rules.pdf')}}" target="_blank" class="main-link">Правилами конкурса</a></p>
                    </div>
                </div>

                <input type="submit" class="btn btn-red" value="Отправить">
            </form>
        </div>
    </div>
</div>

<div class="popup-bg" id="feedback-screen">
    <div class="popup-box">
        <button class="close-popup" screenID="feedback-screen"></button>
        <div class="form-container">
            <div class="popup-title">Обратная связь</div>
            <div class="text-center wait"  style="display:none;"><i class="fa fa-spinner fa-pulse fa-5x" style="color: #d60058;"></i></div>
            <form action="{{route('contact.form')}}" method="post" class="forms form-horizontal feedback-form" id="contact-form">
                {{ csrf_field() }}
                <div class="form-group form-group-email">
                    <div class="col-md-12"  id="email-span">
                        <input type="text" class="form-control" placeholder="Email" name="email">
                    </div>
                </div>
                <div class="form-group form-group-ms">
                    <div class="col-md-12" id="ms-span">
                        <textarea name="message" id="message"  class="form-control" rows="4" placeholder="Ваш вопрос или сообшение"></textarea>
                    </div>
                </div>
                <div class="line-sep-02"></div>
                <p>Заполните все поля и нажмите "Отправить"</p>
                <input type="submit" class="btn btn-red js-contact" value="Отправить">
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
</script>
<script src="{{ asset("js/jquery.min.js") }}"></script>
<script src="{{ asset("js/bootstrap.min.js") }}"></script>
<script src="{{ asset("js/navigation.js") }}"></script>
<script src="{{ asset("js/custom.js") }}?090517-03"></script>

</body>
</html>