<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') | Faberlic | Winner </title>

        <!-- Datatables -->
        <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/responsive.bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/scroller.bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
        <!-- Sweet alert -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/sweetalert.css') }}">
        <link href="{{ asset("css/dropzone.min.css") }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">
        <link href="{{ asset("css/custom.min.css") }}" rel="stylesheet">
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>

        @stack('stylesheets')

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                @include('backend.includes.sidebar')
                @include('backend.includes.topbar')
                <!-- page content -->
                <div class="right_col" role="main">
                    @yield('main_container')
                </div>
                <!-- /page content -->
                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                       Admin Template for Faberlic
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <!-- jQuery -->
        <script src="{{ asset("js/jquery.min.js") }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset("js/bootstrap.min.js") }}"></script>

        <!-- Datatables -->
        <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('js/responsive.bootstrap.js') }}"></script>
        <script src="{{ asset('js/dataTables.scroller.min.js') }}"></script>
        <!-- Dropzone.js -->
        <script src="{{ asset('js/dropzone.min.js') }}"></script>
        <!-- Custom Theme Scripts -->
        <script src="{{ asset("js/gentelella.min.js") }}"></script>
        <script type="text/javascript">
            var APP_URL = {!! json_encode(url('/')) !!};
        </script>
        @stack('scripts')

    </body>
</html>