var table = $('#datatable').DataTable({
bProcessing: false,
"language": {
"search": "Поиск:",
"lengthMenu": "_MENU_ записей",
"zeroRecords": "Ничего не найдено",
"info": "Страница _PAGE_ из _PAGES_",
"infoEmpty": "Нет доступных записей",
"infoFiltered": "(выбрано из _MAX_ записей)",
"paginate": {
"previous": "пред.",
"next": "след.",
}
},
keys: true,
pageLength: 25,
processing: true,
serverSide: true,
ajax: '{!! route('contact_form_requests.data') !!}',
columns: [
{data: 'date', name: 'created_at'},
{data: 'message', name: 'message'},
{data: 'email', name: 'email'},
{data: 'option', name: 'option', orderable: false, searchable: false}
],
initComplete: function () {
var i = 0;
this.api().columns().every(function () {
i++;

var column = this;
var input = document.createElement("input");


if(i != 4) {
$(input).appendTo($(column.header()))
.on('input', function () {
column.search($(this).val(), true, true, true).draw();
});
}

});
}
});
