@extends('layouts.back')
@section('title', $data->get('title'))
@push('stylesheets')@endpush

@section('main_container')
    @include('errors.error')
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{!! $data->get('title') !!} </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <ul class="list-unstyled timeline">
                            <li>
                                <div class="block">
                                    <div class="tags">
                                        <a href="" class="tag">
                                            <span><i class="fa fa-comment-o" aria-hidden="true"></i></span>
                                        </a>
                                    </div>
                                    <div class="block_content">
                                        <div class="byline">

                                            <span><?php Carbon\Carbon::setLocale('ru');echo $record['created_at']->diffForHumans(); ?></span> от <a>{{$record->name}}</a>
                                        </div>
                                        <p class="excerpt">{{$record->message}}</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <a class="btn btn-info pull-right"
                       href="{{URL::route('contact_form.index') }}"
                       role="button" style="margin-right: 5%;"><i class="fa fa-undo"></i> Вернуться
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
@endpush
