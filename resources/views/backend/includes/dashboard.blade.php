@extends('layouts.back')
@section('title', 'Dashboard')
@push('stylesheets')
@endpush

@section('main_container')

    <div class="row top_tiles">
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-users"></i></div>
                <div class="count">{{$participant_all}}</div>
                <h3>участников</h3>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-trophy"></i></div>
                <div class="count">{{$winners}}</div>
                <h3>победителей</h3>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-comment"></i></div>
                <div class="count">{{$contacts}}</div>
                <h3>собщений</h3>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
@stop

@push('scripts')
@endpush

