@extends('layouts.back')
@section('title', $data->get('title'))
@push('stylesheets')@endpush

@section('main_container')

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>{!! $data->get('title') !!}</h4>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="@if(!empty($data->get('features'))) col-md-9 col-sm-9 @else col-md-12 col-sm-12 @endif col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <ul class="nav navbar-left panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        @if(!empty($data->get('str_title')))
                            <h2><small>{{$data->get('str_title')}}</small></h2>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <br />
                        @if(empty($record->id))
                            @if(!empty($data->get('var')))
                                {!!Form::model($record,['route' =>[$data->get('route'), $data->get('var')],
                                'data-parsley-validate',
                                'enctype'=>'multipart/form-data',
                                'class'=>'form-horizontal form-label-left'])!!}
                            @else
                                {!!Form::model($record,['route' =>[$data->get('route')],
                          'data-parsley-validate',
                          'enctype'=>'multipart/form-data',
                          'class'=>'form-horizontal form-label-left'])!!}
                            @endif
                        @else
                            @if(!empty($data->get('var')))
                                {!! Form::model($record, ['method' => 'PATCH',
                            'route' => [$data->get('route'), $data->get('var'),$record->id],

                            'data-parsley-validate','enctype'=>'multipart/form-data',
                            'class'=>'form-horizontal form-label-left']) !!}

                            @else
                                {!! Form::model($record, ['method' => 'PATCH',
                            'route' => [$data->get('route'),$record->id],

                            'data-parsley-validate','enctype'=>'multipart/form-data',
                            'class'=>'form-horizontal form-label-left']) !!}

                            @endif
                        @endif
                        @include( $data->get('include'),  ['submit_text' => $data->get('submit_text')])

                        @if(!empty($data->get('features')))
                            @if(empty($record->id))
                                <input type="hidden" name="{{$data->get('feature_name')}}" value="" id="hidden_input">
                            @else
                                <input type="hidden" name="{{$data->get('feature_name')}}" value="<?php $name = $data->get('feature_name'); echo $record[$name] ?>" id="hidden_input">
                            @endif
                        @endif
                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
            @if(!empty($data->get('features')))
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Минюатюра <small>записи</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content img_form">
                            <br />
                            @if(empty($record->id))
                                {!!Form::model(new App\Models\Media, ['route' =>  $data->get('route_feature'),
                                                                 'data-parsley-validate','files'=>true,
                                                                 'enctype'=>'multipart/form-data',
                                                                 'class'=>'dropzone', 'id'=>'my-dropzone'])!!}
                                {{ Form::close() }}
                            @elseif(!empty($record->id) && empty($record->media->id) )
                                {!!Form::model(new App\Models\Media, ['route' =>  'media.store',
                                                                     'data-parsley-validate','files'=>true,
                                                                     'enctype'=>'multipart/form-data',
                                                                     'class'=>'dropzone', 'id'=>'my-dropzone'])!!}
                                {{ Form::close() }}
                            @else
                                {{ Form::open(array('route' => array('media.destroy', $record->media->id), 'method' => 'delete', 'class'=>'','id'=>'img-del', 'data-parsley-validate','files'=>true )) }}

                                <img src="{{url('/uploads/admin/'.$record->media->title)}}" class="img-thumbnail"/>
                                <a data-del = "" class="btn btn-link pull-right delete-img">Удалить</a>
                                {{ Form::close() }}

                                {!!Form::model(new App\Models\Media, ['route' =>  'media.store',
                                                                'data-parsley-validate','files'=>true,
                                                                'enctype'=>'multipart/form-data',
                                                                'class'=>'dropzone ', 'id'=>'my-dropzone', 'style'=>'display: none'])!!}
                                {{ Form::close() }}
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop

@push('scripts')
<script>

    /*DropZone */
    var fileList = new Array;
    Dropzone.options.myDropzone = {
        maxFiles: 1,
        paramName: "image",
        maxFilesize: 5,
        addRemoveLinks: true,
        dictRemoveFile: 'удалить',
        acceptedFiles: '.jpg, .jpeg, .png',
        dictFileTooBig: 'Image is bigger than 5MB',

        accept: function(file, done) {
            done();
        },
        success: function(file, response){
            $('#hidden_input').val(response.id);
            fileList = {"serverFileName" : response.title, "fileName" : response.name,"fileId" : response.id};
        },
        init: function() {
            this.on("maxfilesexceeded", function(file){
                alert("Не больше одного файла, пожалуйста");
            });

            this.on("removedfile", function(file) {
                var rmvFile = "";
                if(fileList.fileName == file.name)
                {
                    rmvFile = fileList.fileId;
                }

                if (rmvFile){
                    var url = APP_URL + '/admin/upload/delete';
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: rmvFile, _token: $('meta[name="csrf-token"]').attr('content')},
                        dataType: 'html',
                        success: function(data){
                            $('#hidden_input').val('');
                        }
                    });

                }
            } );
        }

    };

    $('a.delete-img').on('click', function () {

        var form = $('#img-del');
        var form1 = $('#my-dropzone');
        var rowRoute = form.attr("action");
        var rowToken = $('meta[name="csrf-token"]').attr('content');
        var img = $('.img-thumbnail');
        deleteImg(rowRoute, rowToken, img);
        function deleteImg(rowRoute, rowToken, img)
        {
            $.ajax({
                headers: { 'X-CSRF-TOKEN' : rowToken},
                url: rowRoute,
                type: "POST",
                data: { _method:"DELETE" },

                success: function() {
                    form.remove();
                    form1.css('display','block');
                    $('#hidden_input').val('');
                },
                error: function() {
                    swal("Oops", "The Connection to the Server Failed", "Error");
                }
            })

        }
    });

</script>
@endpush

