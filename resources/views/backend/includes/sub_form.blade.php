{!! Form::model($data->get('header'), ['method' => 'PATCH',
'route' => ['headers.update', $data->get('header')->id],
'data-parsley-validate','enctype'=>'multipart/form-data',
'class'=>'form-horizontal form-label-left']) !!}
<div class="input-group">
    {!! Form::text('title', $data->get('header')['title'], ['class'=>'form-control', 'placeholder'=>'Заголовок секции', 'id'=>'', 'required'] ) !!}
    <span class="input-group-btn"><button  type="submit" class="btn btn-primary">Изменить заголовок</button></span>
</div>
{!!Form::close()!!}