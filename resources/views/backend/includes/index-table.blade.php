@extends('layouts.back')
@section('title', $data->get('title'))
@push('stylesheets')@endpush

@section('main_container')
    @include('errors.error')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>{!! $data->get('title') !!}<span id="wait" style="display: inline-block"></span></h4>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="alert_ajax"></div>
        @if(session('alert'))
            <div class="alert alert-success alert-dismissible fade in" role="alert" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong>{{ session('alert') }} </strong>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                        @if(!empty($data->get('route')))

                        <div class="col-md-12 col-sm-12 col-xs-12">
                                {{ Form::open(array('route' => array($data->get('route')), 'method' => 'get', 'id'=>'get_winner','style'=>'display:inline-table')) }}
                                {{ csrf_field() }}
                                <input type="hidden" name="winner" value="">
                                {!! Form::button( $data->get('submit_text'), ['type' => 'submit','class'=>'btn btn btn-success pull-left']) !!}
                                {{ Form::close() }}

                            {{ Form::open(array('route' => array($data->get('route')), 'method' => 'get', 'id'=>'get_all', 'style'=>'display:none')) }}
                            {{ csrf_field() }}
                            <input type="hidden" name="winner" value="1">
                            {!! Form::button('все участники', ['type' => 'submit','class'=>'btn btn-primary pull-left']) !!}
                            {{ Form::close() }}
                        </div>
                        @endif
                        <div class="clearfix"></div>

                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            @include('backend.includes.table-head')
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script>
    $(document).ready(function ($) {
        $('#alert').delay(3000).slideUp();
    });
    $(function() {
        @include($data->get('include'))
        $(document).on('click', 'a.deleteRow', function () {
            var rowRoute = $(this).attr("data-row_destroy_route");
            var rowToken =$("#token").attr('content');
            deleteRow(rowRoute, rowToken, table);
        });

        $(document).on('click', 'a.sendEmail', function () {
            var rowRoute = $(this).attr("data-row_send_route");
            var rowToken =$("#token").attr('content');
            $('#wait').empty().css('display', 'inline-block').html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>');
            $(this).addClass('disabled');
            sendEmail(rowRoute, rowToken, table);

        });
        function sendEmail(rowRoute, rowToken, table)
        {
            $.ajax({
                headers: { 'X-CSRF-TOKEN' : rowToken},
                url: rowRoute,
                type: "POST",
                success: function(data) {
                    $('#wait').empty().css('display', 'none');
                    $('#alert_ajax').empty().html('<div class="alert alert-success alert-dismissible fade in" role="alert" id="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>Сообщение успешно отправлено</strong></div>').delay(1000).slideUp();
                    table.ajax.reload( null, false );
                },
                error: function(data) {
                    swal("Oops", "Connection failed", "Error");
                    $('a.sendEmail').removeClass('disabled');
                }
            })
        }
        function deleteRow(rowRoute, rowToken, table)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "Cancel",
                confirmButtonColor: "#ec6c62"
            }, function()
            {
                $.ajax({
                    headers: { 'X-CSRF-TOKEN' : rowToken},
                    url: rowRoute,
                    type: "POST",
                    data: { _method:"DELETE" },
                    success: function(data) {
                        $('#alert_ajax').html('<div class="alert alert-success alert-dismissible fade in" role="alert" id="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>Record has been deleted</strong></div>').delay(3000).slideUp();
                        table.ajax.reload( null, false );
                    },
                    error: function(data) {
                        swal("Oops", "Connection failed", "Error");
                    }
                })
            });
        }


    });
</script>
@endpush