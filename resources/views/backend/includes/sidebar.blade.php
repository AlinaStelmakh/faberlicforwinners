<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/') }}" class="site_title"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<span>Faberlic</span></a>
        </div>
        
        <div class="clearfix"></div>

        <br />
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>&nbsp;</h3>
                <ul class="nav side-menu">
                    <li><a href="{{route('winners.index')}}"><i class="fa fa-trophy"></i>Победители</a></li>
                    <li><a href="{{route('contact_form.index')}}"><i class="fa fa-comment-o"></i>Сообщения</a></li>
                    <li><a href="{{route('dashboard')}}"><i class="fa fa-tachometer"></i>Итоги</a></li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
        
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>