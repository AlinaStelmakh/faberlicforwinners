var table = $('#datatable').DataTable({
bProcessing: false,
order: [[5, 'desc' ]],
keys: true,
pageLength: 100,
processing: true,
serverSide: true,
ajax: {
url: '{!! route('winners.data') !!}',
data: function (d) {
d.winner = $('input[name=winner]').val();
}
},
order: [ 0, 'desc' ],
columns: [

{data: 'id', name: 'id', width: '100px'},
{data: 'name', name: 'name'},
{data: 'email', name: 'email'},
{data: 'ref', name: 'ref'},
{data: 'date', name: 'date'},
{data: 'url', name: 'url', orderable: false, searchable: false, width: '100px'},
{data: 'status', name: 'status', width: '150px'},
{data: 'option', name: 'option', orderable: false, searchable: false, width: '100px'}
],
initComplete: function () {
var i = 0;
this.api().columns().every(function () {
i++;

var column = this;
var input = document.createElement("input");input.size = 10;

if(i < 6){
$(input).appendTo($(column.header()))
.on('input', function () {
column.search($(this).val(), true, true, true).draw();
});

$(input).on( 'click', function(e) { e.stopPropagation(); } );
}else if(i == 7) {
var select = $('<select><option value="" >все</option></select>').appendTo( $(column.header()) )
.on( 'change', function () {
var val = $.fn.dataTable.util.escapeRegex(
$(this).val()
);

column
.search( val ? '^'+val+'$' : '', true, false )
.draw();
} );

column.data().unique().sort().each( function ( d, j ) {
var select_val;
if(d =='зарегистрированный'){
select_val =  0;
}else{
select_val =  1;
}
select.append( '<option value="'+select_val+'">'+d+'</option>' )
} );
}

});
}
});

$('#get_winner').on('submit', function(e) {
$('input[name=winner]').val(1);
table.draw();
e.preventDefault();
$('#get_all').css('display','inline-table')
});
$('#get_all').on('submit', function(e) {
$('input[name=winner]').val(2);
table.draw();
e.preventDefault();
$('#get_all').css('display','none')
});