<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Имя<span class="required"> *</span></label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {!! Form::text('name', $record['name'], ['class'=>'form-control', 'placeholder'=>'', 'id'=>'name_user', 'required'] ) !!}
    </div>
    @if ($errors->has('name'))
        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">E-mail<span class="required"> *</span></label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {!! Form::email('email', $record['email'], ['class'=>'form-control', 'placeholder'=>'', 'id'=>'', 'required',] ) !!}
        @if ($errors->has('email'))
            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Телефон<span class="required"> *</span></label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {!! Form::text('phone', $record['phone'], ['class'=>'form-control', 'placeholder'=>'', 'id'=>'', 'required',] ) !!}
        @if ($errors->has('phone'))
            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Город<span class="required"> *</span></label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {!! Form::text('city', $record['city'], ['class'=>'form-control', 'placeholder'=>'', 'id'=>'', 'required',] ) !!}
        @if ($errors->has('city'))
            <span class="help-block"><strong>{{ $errors->first('city') }}</strong></span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('consult_number') ? ' has-error' : '' }}">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Номер консультанта<span class="required"> *</span></label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {!! Form::text('consult_number', $record['consult_number'], ['class'=>'form-control', 'placeholder'=>'', 'id'=>'', 'required',] ) !!}
        @if ($errors->has('consult_number'))
            <span class="help-block"><strong>{{ $errors->first('consult_number') }}</strong></span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}{{ $errors->has('count') ? ' has-error' : '' }}">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">URL<span class="required"> *</span></label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {!! Form::url('url', $record['url'], ['class'=>'form-control', 'placeholder'=>'', 'id'=>'', 'required',] ) !!}
        @if ($errors->has('url'))
            <span class="help-block"><strong>{{ $errors->first('url') }}</strong></span>
        @elseif  ($errors->has('count'))
            <span class="help-block"><strong>{{ $errors->first('count') }}</strong></span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('ref') ? ' has-error' : '' }}">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Артикль<span class="required"> *</span></label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {!! Form::text('ref', $record['ref'], ['class'=>'form-control', 'placeholder'=>'', 'id'=>'', 'required',] ) !!}
        @if ($errors->has('ref'))
            <span class="help-block"><strong>{{ $errors->first('ref') }}</strong></span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Статус<span class="required"> *</span></label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {!! Form::select('status', array('0' => 'зарегистрированный', '1' => 'победитель'), $record['status'], ['class'=>'form-control', 'id'=>'']) !!}
        @if ($errors->has('status'))
            <span class="help-block"><strong>{{ $errors->first('status') }}</strong></span>
        @endif
    </div>
</div>


<div class="form-group">
    <div class="col-md-12 col-sm-12 col-xs-12">
        {!! Form::button($submit_text, ['type' => 'submit','class'=>'btn btn-success pull-right']) !!}
        <a class="btn btn-info pull-right"
           href="{{URL::route($data->get('route_cancel')) }}"
           role="button" style="margin-right: 2%;">отмена
        </a>
    </div>
</div>

